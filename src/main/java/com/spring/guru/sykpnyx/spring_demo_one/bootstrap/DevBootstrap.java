package com.spring.guru.sykpnyx.spring_demo_one.bootstrap;

import com.spring.guru.sykpnyx.spring_demo_one.model.Author;
import com.spring.guru.sykpnyx.spring_demo_one.model.Book;
import com.spring.guru.sykpnyx.spring_demo_one.model.Publisher;
import com.spring.guru.sykpnyx.spring_demo_one.repositories.AuthorRepository;
import com.spring.guru.sykpnyx.spring_demo_one.repositories.BookRepository;
import com.spring.guru.sykpnyx.spring_demo_one.repositories.PublisherRepository;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
  private AuthorRepository authorRepository;
  private BookRepository bookRepository;
  private PublisherRepository publisherRepository;

  private void initData() {

    Publisher publisher = new Publisher();
    publisher.setName("Foo");

    publisherRepository.save(publisher);

    Author eric = new Author("Eric", "Evans");
    Book ddd = new Book("Domain Driven Design", "1234", publisher);
    eric.getBooks().add(ddd);
    ddd.getAuthors().add(eric);

    authorRepository.save(eric);
    bookRepository.save(ddd);

    Author rod = new Author("Rod", "Johnson");
    Book noEJB = new Book("J2EE Development without EJB", "1114", publisher);
    rod.getBooks().add(noEJB);
    noEJB.getAuthors().add(rod);

    authorRepository.save(rod);
    bookRepository.save(noEJB);
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    initData();

  }

  public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository,
      PublisherRepository publisherRepository) {
    this.authorRepository = authorRepository;
    this.bookRepository = bookRepository;
    this.publisherRepository = publisherRepository;
  }
}