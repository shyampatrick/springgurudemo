package com.spring.guru.sykpnyx.spring_demo_one.repositories;

import com.spring.guru.sykpnyx.spring_demo_one.model.Author;

import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
