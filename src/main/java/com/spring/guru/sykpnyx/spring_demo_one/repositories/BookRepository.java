package com.spring.guru.sykpnyx.spring_demo_one.repositories;

import com.spring.guru.sykpnyx.spring_demo_one.model.Book;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {

}
