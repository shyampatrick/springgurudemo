package com.spring.guru.sykpnyx.spring_demo_one.repositories;

import com.spring.guru.sykpnyx.spring_demo_one.model.Publisher;

import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
